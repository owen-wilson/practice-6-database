CREATE SCHEMA dbo;

CREATE TABLE bitacora_persona ( 
	persona_id_bitacora  smallint NOT NULL   ,
	fecha                date NOT NULL   ,
	descripcion          varchar(100) NOT NULL   ,
	usuario              varchar(30) NOT NULL   
 );

CREATE TABLE persona ( 
	persona_id           smallint NOT NULL   ,
	nombre               varchar(50) NOT NULL   ,
	apellido             varchar(50)    ,
	genero               char(1) NOT NULL   ,
	fechaNacimiento      date    ,
	pais                 varchar(20)    ,
	CONSTRAINT Pk_persona_persona_id PRIMARY KEY  ( persona_id )
 );

CREATE TABLE libro ( 
	libro_id             smallint NOT NULL   ,
	titulo               varchar(50) NOT NULL   ,
	precio               decimal(5,2)    ,
	persona_id           smallint NOT NULL   ,
	CONSTRAINT Pk_libro_libro_id PRIMARY KEY  ( libro_id )
 );

CREATE  INDEX Idx_libro_persona_id ON libro ( persona_id );

CREATE TRIGGER myBitacora
ON persona 
FOR INSERT
AS
DECLARE @cod_persona smallint
SELECT @cod_persona = persona_id FROM INSERTED
INSERT INTO bitacora_persona VALUES (@cod_persona,getdate(),'registro nuevo',system_user)

ALTER TABLE libro ADD CONSTRAINT fk_libro_persona FOREIGN KEY ( persona_id ) REFERENCES persona( persona_id );

INSERT INTO bitacora_persona( persona_id_bitacora, fecha, descripcion, usuario ) VALUES ( 7, '2018-09-05', 'registro nuevo', 'sa' ); 

INSERT INTO persona( persona_id, nombre, apellido, genero, fechaNacimiento, pais ) VALUES ( 1, 'pepito', 'perez', 'M', '1992-02-02', 'canada' ); 
INSERT INTO persona( persona_id, nombre, apellido, genero, fechaNacimiento, pais ) VALUES ( 2, 'micaela', '', 'F', '1995-05-06', 'bolivia' ); 
INSERT INTO persona( persona_id, nombre, apellido, genero, fechaNacimiento, pais ) VALUES ( 3, 'sara', 'fernandez', 'F', '1996-12-30', '' ); 
INSERT INTO persona( persona_id, nombre, apellido, genero, fechaNacimiento, pais ) VALUES ( 4, 'juancito', 'pinto', 'M', '1990-12-25', 'peru' ); 
INSERT INTO persona( persona_id, nombre, apellido, genero, fechaNacimiento, pais ) VALUES ( 5, 'carla', 'toranzos', 'F', '1992-08-18', 'bolivia' ); 
INSERT INTO persona( persona_id, nombre, apellido, genero, fechaNacimiento, pais ) VALUES ( 6, 'diana', 'vargas', 'F', '1996-02-02', 'bolivia' ); 
INSERT INTO persona( persona_id, nombre, apellido, genero, fechaNacimiento, pais ) VALUES ( 7, 'laura', 'paredes', 'F', '1996-07-02', 'bolivia' ); 

INSERT INTO libro( libro_id, titulo, precio, persona_id ) VALUES ( 1, 'matematicas', 26.55, 5 ); 
INSERT INTO libro( libro_id, titulo, precio, persona_id ) VALUES ( 2, 'filosofia', 15.25, 3 ); 
INSERT INTO libro( libro_id, titulo, precio, persona_id ) VALUES ( 3, 'calculo diferencial', null, 4 ); 
INSERT INTO libro( libro_id, titulo, precio, persona_id ) VALUES ( 4, 'fisica', 12.25, 2 ); 
INSERT INTO libro( libro_id, titulo, precio, persona_id ) VALUES ( 5, 'algebra', 11.23, 1 ); 

