--ejemplo usado para la tabla persona

CREATE TRIGGER myBitacora
ON persona 
FOR INSERT
AS
DECLARE @cod_persona smallint
SELECT @cod_persona = persona_id FROM INSERTED
INSERT INTO bitacora_persona VALUES (@cod_persona,getdate(),'registro nuevo',system_user)

SELECT * FROM persona

insert into persona (persona_id,nombre,apellido,genero,fechaNacimiento,pais) values (7,'laura','paredes','F','1996/07/02','bolivia')

SELECT * FROM bitacora_persona
